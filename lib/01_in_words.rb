class Fixnum

  NUM_WORD_HASH = {
    0 => "zero",
    1 => "one",
    2 => "two",
    3 => "three",
    4 => "four",
    5 => "five",
    6 => "six",
    7 => "seven",
    8 => "eight",
    9 => "nine",
    10 => "ten",
    11 => "eleven",
    12 => "twelve",
    13 => "thirteen",
    14 => "fourteen",
    15 => "fifteen",
    16 => "sixteen",
    17 => "seventeen",
    18 => "eighteen",
    19 => "nineteen",
    20 => "twenty",
    30 => "thirty",
    40 => "forty",
    50 => "fifty",
    60 => "sixty",
    70 => "seventy",
    80 => "eighty",
    90 => "ninety",
    100 => "hundred",
    1_000 => "thousand",
    1_000_000 => "million",
    1_000_000_000 => "billion",
    1_000_000_000_000 => "trillion"
  }.freeze

  def in_words
    case self
    when 0..9
      self.ones
    when 10..99
      self.tens
    when 100..999
      self.hundreds
    when 1_000..999_999
      self.thousands
    when 1_000_000..999_999_999
      self.millions
    when 1_000_000_000..999_999_999_999
      self.billions
    when 1_000_000_000_000..999_999_999_999_999
      self.trillions
    end

  end

  def ones
    NUM_WORD_HASH[self % 10]
  end

  def tens
    tens_arr = []
    if self < 20
      return NUM_WORD_HASH[self]
    else
      return NUM_WORD_HASH[self] if self % 10 == 0
      tens_arr << NUM_WORD_HASH[self / 10 * 10] << NUM_WORD_HASH[self % 10]
    end
    tens_arr.join(' ')
  end

  def hundreds
    hundreds_arr = []
    hundreds_arr << NUM_WORD_HASH[self / 100] << NUM_WORD_HASH[100]
    return hundreds_arr.join(' ') if self % 100 == 0
    hundreds_arr << (self % 100).tens
    hundreds_arr.join(' ')
  end

  def thousands
    thousands_arr = []
    thousands_arr << self.front_digits(1_000) if self / 1_000 != 0
    return thousands_arr.join(' ') if self % 1_000 == 0

    case hundred = self % 1_000
    when 1..99
      thousands_arr << hundred.tens
    when 100..999
      thousands_arr << hundred.hundreds
    end

    thousands_arr.join(' ')
  end

  def millions
    millions_arr = []
    if self / 1_000_000 != 0
      millions_arr << self.front_digits(1_000_000)
    end

    return millions_arr.join(' ') if self % 1_000_000 == 0
    millions_arr << (self % 1_000_000).thousands
    millions_arr.join(' ')
  end

  def billions
    billions_arr = []
    if self / 1_000_000_000 != 0
      billions_arr << self.front_digits(1_000_000_000)
    end

    return billions_arr.join(' ') if self % 1_000_000_000 == 0
    billions_arr << (self % 1_000_000_000).millions
    billions_arr.join(' ')
  end

  def trillions
    trillions_arr = []
    if self / 1_000_000_000_000 != 0
      trillions_arr << self.front_digits(1_000_000_000_000)
    end

    return trillions_arr.join(' ') if self % 1_000_000_000_000 == 0
    trillions_arr << (self % 1_000_000_000_000).billions
    trillions_arr.join(' ')
  end

  def front_digits(place)
    arr = []
    case front = self / place
    when 1..99
      arr << front.tens << NUM_WORD_HASH[place]
    when 100..999
      arr << front.hundreds << NUM_WORD_HASH[place]
    end
    arr.join(' ')
  end

end
